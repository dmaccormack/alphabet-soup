package net.maccormack.alphasoup;

import java.util.Iterator;

/**
 * An immutable grid of words.
 *
 * @author dmaccormack
 */
final class WordGrid {
    /**
     * Character data.
     */
    private final char[][] data;

    /**
     * Width of the grid.
     */
    private final int width;

    /**
     * Create a new WordGrid.
     */
    private WordGrid(int width, char[][] data) {
        this.data = data;
        this.width = width;
    }

    /**
     * Build a new word grid.
     * @param text non-null String List
     */
    public static WordGrid build(int width, int height, Iterator<String> text) {
        //
        // Note, error handling omitted per allowance in the instructions.
        //
        char[][] data = new char[height][width];

        for (int row = 0; text.hasNext(); row++) {
            String line = text.next();
            int col = 0;

            for (int index = 0; index < line.length(); index++) {
                char c = line.charAt(index);
                if (!Character.isWhitespace(c)) {
                    data[row][col] = Character.toLowerCase(c);
                    col++;
                }
            }
        }
        
        return new WordGrid(width, data);
    }

    /**
     * Locate the given word in this grid.
     * @param word a non-null String
     * @return a non-null HitOrMiss.
     */
    public HitOrMiss locate(String word) {
        HitOrMiss ret = new HitOrMiss(word);

        if (word == null || word.length() == 0) {
            return ret;
        }

        String norm = normalize(word);
        String rnorm = reverse(norm);

        for (int row = 0; row < data.length; row++) {
            if (locateRow(ret, norm, row, false)
                    || locateRow(ret, rnorm, row, true)) {
                return ret;
            }
        }
        for (int col = 0; col < width; col++) {
            if (locateCol(ret, norm, col, false)
                    || locateCol(ret, rnorm, col, true)) {
                return ret;
            }
        }

        final int normLength = norm.length();
        for (int row = 0; row + normLength <= data.length; row++) {
            if (locateDiag(ret, norm, row, false)
                    || locateDiag(ret, rnorm, row, true)) {
                return ret;
            }
        }
        return ret;
    }
    
    private boolean locateRow(
            HitOrMiss hom,
            String word,
            int rowIndex,
            boolean isReversed) {

        char[] row = data[rowIndex];
        int length = word.length();

        //
        // loop across each index in the row looking for a match. I can stop
        // looking if the word is longer than the remaining number of chars in
        // the row (because words don't wrap).
        //
        for (int col = 0; col + length <= row.length; col++) {
            if (isEqualRow(word, row, col, length)) {
                int endPos = col + length - 1;
                hom.setLocation(
                    rowIndex,
                    isReversed ? endPos : col,
                    rowIndex,
                    isReversed ? col : endPos);
                return true;
            }
        }
        return false;
    }
    
    private boolean locateCol(
            HitOrMiss hom,
            String word,
            int colIndex,
            boolean isReversed) {

        int length = word.length();

        //
        // loop across each row index looking for a match on the current
        // column. I can stop when the word is longer than number of remaining
        // rows.
        //
        for (int row = 0; row + length <= data.length; row++) {
            if (isEqualCol(word, data, colIndex, row, length)) {
                int endPos = row + length - 1;
                hom.setLocation(
                    isReversed ? endPos : row,
                    colIndex,
                    isReversed ? row : endPos,
                    colIndex);
                return true;
            }
        }
        return false;
    }
    
    /**
     * Test if {@code s} is equal to the word formed by {@code chars},
     * starting at {@code start} and walking a diagonal.
     */
    private boolean locateDiag(
            HitOrMiss hom,
            String word,
            int row,
            boolean isReversed) {

        int length = word.length();

        //
        // look for the word on a diagonal, for all columns of the given row.
        //
        for (int col = 0; col + length <= width; col++) {
            if (isEqualDiag(word, data, row, col, length)) {
                setDiagLocation(hom, row, col, length, isReversed, false);
                return true;
            }
        }

        //
        // look for the word on a diagonal in reverse, for all columns of the
        // given row.
        //
        for (int col = width - 1; col >= length - 1; col--) {
            if (isEqualDiagRev(word, data, row, col, length)) {
                setDiagLocation(hom, row, col, length, isReversed, true);
                return true;
            }
        }
        return false;
    }

    /**
     * Set the location for a call to
     * {@link #locateDiag(HitOrMiss, String, int, boolean)}.
     */
    private static void setDiagLocation(
            HitOrMiss hom,
            int row,
            int col,
            int length,
            boolean isWordReversed,
            boolean isDiagReversed) {

        int rowEnd = row + length - 1;
        int colEnd = isDiagReversed ? col - (length - 1) : col + length - 1;

        hom.setLocation(
            isWordReversed ? rowEnd : row,
            isWordReversed ? colEnd : col,
            isWordReversed ? row : rowEnd,
            isWordReversed ? col : colEnd);            
    }

    /**
     * Test if {@code s} is equal to the word formed by {@code chars},
     * starting at {@code start} and walking across the row.
     */
    private static boolean isEqualRow(
            String s,
            char[] chars,
            int start,
            int length) {

        int end = start + length;

        for (int ci = start, i = 0; ci < end; ci++, i++) {
            if (chars[ci] != s.charAt(i)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Test if {@code s} is equal to the word formed by {@code chars},
     * starting at {@code start} and walking down the column.
     */
    private static boolean isEqualCol(
            String s,
            char[][] chars,
            int colIndex,
            int start,
            int length) {

        int end = start + length;

        for (int ri = start, i = 0; ri < end; ri++, i++) {
            if (chars[ri][colIndex] != s.charAt(i)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Test if {@code s} is equal to the word formed by {@code chars},
     * starting at {@code start} and walking a diagonal.
     */
    private static boolean isEqualDiag(
            String s,
            char[][] chars,
            int row,
            int col,
            int length) {
    
        for (int i = 0; i < length; i++, col++, row++) {
            if (chars[row][col] != s.charAt(i)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Test if {@code s} is equal to the word formed by {@code chars},
     * starting at {@code start} and walking a diagonal in reverse.
     */
    private static boolean isEqualDiagRev(
            String s,
            char[][] chars,
            int row,
            int col,
            int length) {
    
        for (int i = 0; i < length; i++, col--, row++) {
            if (chars[row][col] != s.charAt(i)) {
                return false;
            }
        }
        return true;
    }
    
    /**
     * Normalize the given text. A word in the grid will not have spaces,
     * even if the "real" word does. This method applies a transformation
     * to the given text so that it will match a word as it will appear in
     * the grid.
     *
     * @param text a non-null String
     * @return a normalized string
     */
    private static String normalize(String text) {
        StringBuilder ret = new StringBuilder(text.length());

        for (int i = 0; i < text.length(); i++) {
            char c = text.charAt(i);
            if (!Character.isWhitespace(c)) {
                ret.append(Character.toLowerCase(c));
            }
        }

        return ret.toString();
    }

    /**
     * Reverse the given string.
     */
    private static String reverse(String text) {
        return new StringBuilder(text).reverse().toString();
    }
}



