package net.maccormack.alphasoup;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * A service to solve alphabet soup. All of the business logic is encapsulated
 * here.
 * @author dmaccormack
 */
public class Solver {
    /**
     * Regex for parsing the grid size.
     */
    private static final Pattern SIZE_PATTERN = Pattern.compile(
        "\\s*(\\d+)\\s*[xX]\\s*(\\d+)\\s*");

    /**
     * Solve the alphabet soup.
     *
     * <p>There are a number of ways of going about this. I provided the
     * naive implementation because I think that's what you're looking for,
     * in order to evaluate basic code structure; however, I figured it's worth
     * noting some alternatives.
     * <ol><li>A straightforward, small improvement would be to create a
     * HashMap&lt;String,HitOrMiss&gt; where the key is a normalized string, and
     * insert both forward and reversed values pointing to the same HitOrMiss.
     * The WordGrid could implement an Iterator/Stream that accepts an integer
     * (word length) and generates all possible words across columns, rows, and
     * diagonals. The solver would then iterate those values for each distinct
     * `to-find` word length.
     * Pro: complexity can be modeled as a distribution of the length of the
     * `to-find` words.
     * Con: it'll use more memory, both for the HashMap itself and at query time
     * because it has to build instances of String to lookup each value in the
     * HashMap.
     * <li>Use a suffix trie. If the trie contained all rows, columns, and
     * diagonals, and the lookup operation tested the normalized word test,
     * both forward and reversed, then complexity would be O(1).
     * Pro: fast
     * Con: more complex code, more memory at runtime
     * </ol>
     * 
     * @param input stream of Strings
     * @return stream of hits
     */
    public Stream<HitOrMiss> solve(Stream<String> input) {
        //
        // the Java Stream interface doesn't lend itself well to slicing
        //
        List<String> lines = input.collect(Collectors.toList());

        //
        // get the grid size from the first line
        //
        Matcher match = SIZE_PATTERN.matcher(lines.get(0));
        if (!match.matches()) {
            throw new IllegalArgumentException("invalid size specification");
        }

        int height = Integer.parseInt(match.group(1));
        int width = Integer.parseInt(match.group(2));

        //
        // build a WordGrid
        //
        WordGrid wordGrid = WordGrid.build(
            width,
            height,
            lines.subList(1, height + 1).iterator());

        //
        // return a stream of Hits for each word that has a location in the grid
        //
        return lines.subList(height + 1, lines.size())
            .stream()
            .map(wordGrid::locate)
            .filter(HitOrMiss::isHit);
    }
}
