package net.maccormack.alphasoup;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

/**
 * This class implements the CLI interface.
 * @author dmaccormack
 */
@SuppressWarnings({"PMD.UseUtilityClass"})
public final class Main {

    /**
     * File I/O charset.
     */
    public static final Charset FILE_CHARSET = StandardCharsets.UTF_8;

    /**
     * File I/O charset name.
     */
    public static final String FILE_CHARSET_NAME = "UTF-8";

    /**
     * Dash character as a string.
     */
    private static final String DASH = "-";

    /**
     * Limit instantiation to {@link Main#main(String[]) main}.
     */
    private Main() {
    }

    /**
     * Run the application. This method will read and parse the input text,
     * build the necessary data structure(s), and render the output.
     */
    @SuppressWarnings({"PMD.SystemPrintln", "java:S106"})
    private void run(Stream<String> input) {
        new Solver().solve(input).forEach(System.out::println);
    }

    /**
     * Test if the given `argv` represents stdin.
     */
    private static boolean isStdin(String... args) {
        return args.length == 0 || DASH.equals(args[0]);
    }

    /**
     * Get a Stream of Strings for stdin.
     */
    private static Stream<String> stdinStream() {
        return new BufferedReader(new InputStreamReader(
            System.in, FILE_CHARSET)).lines();
    }

    /**
     * OS entry point.
     *
     * <p>Reads data from stdin or a given filename. Assumes input is UTF-8
     * (requirements state input is limited to US-ASCII).
     * 
     * @param args CLI arguments
     * @throws IOException Ordinarily one would catch IO exceptions and provide
     *     a more friendly UX.
     */
    public static void main(final String[] args) throws IOException {
        final Main main = new Main();
        main.run(isStdin(args) ? stdinStream() : Files.lines(Paths.get(args[0])));
    }
}
