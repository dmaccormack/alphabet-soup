/**
 * This package contains my implementation of Enlighten's Alphabet Soup
 * challenge.
 *
 * @see <a href="https://gitlab.com/enlighten-challenge/alphabet-soup"
 * >alphabet-soup GitLab repo</a>
 */
package net.maccormack.alphasoup;