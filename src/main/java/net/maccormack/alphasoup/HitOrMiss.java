package net.maccormack.alphasoup;

import java.util.Objects;

/**
 * A word and it's optional location. If it has a location then it's a
 * <em>hit</em>; otherwise, it's a <em>miss</em>.
 *
 * @author dmaccormack
 */
public class HitOrMiss {

    /**
     * The word.
     */
    private final String word;
    
    /**
     * The X position of the first letter.
     */
    private int firstX;

    /**
     * The Y position of the first letter.
     */
    private int firstY;
    
    /**
     * The X position of the last letter.
     */
    private int lastX;

    /**
     * The Y position of the last letter.
     */
    private int lastY;

    /**
     * True if a location is set.
     */
    private boolean hasLocation;

    /**
     * Create a new HitOrMiss.
     * @param word a non-null String
     */
    public HitOrMiss(final String word) {
        this.word = word;
    }
    
    /**
     * Create a new HitOrMiss.
     * @param word a non-null String
     * @see #setLocation(int, int, int, int)
     */
    public HitOrMiss(
            final String word,
            int firstY,
            int firstX,
            int lastY,
            int lastX) {
        this.word = word;
        this.setLocation(firstY, firstX, lastY, lastX);
    }

    /**
     * Get the word.
     * @return a non-null String
     */
    public String getWord() {
        return word;
    }

    /**
     * Set the position of the hit.
     *
     * @param firstX first letter location on the {@code X}-axis
     * @param firstY first letter location on the {@code Y}-axis
     * @param lastX last letter location on the {@code X}-axis
     * @param lastY last letter location on the {@code Y}-axis
     */
    public void setLocation(int firstY, int firstX, int lastY, int lastX) {
        this.firstX = firstX;
        this.firstY = firstY;
        this.lastX = lastX;
        this.lastY = lastY;
        this.hasLocation = true;
    }

    @Override
    public String toString() {
        return String.format("%s %d:%d %d:%d",
            word, firstY, firstX, lastY, lastX);
    }

    /**
     * Return {@code true} if the word is a hit and has a location.
     */
    public boolean isHit() {
        return hasLocation;
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            firstX,
            firstY,
            lastX,
            lastY,
            word);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof HitOrMiss)) {
            return false;
        }
        HitOrMiss other = (HitOrMiss) obj;
        return firstX == other.firstX && firstY == other.firstY
            && lastX == other.lastX && lastY == other.lastY && Objects.equals(
                word, other.word);
    }

}
