package net.maccormack.alphasoup;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

import java.util.Arrays;
import java.util.stream.Stream;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

/**
 * WordGrid Unit Test.
 * @author dmaccormack
 */
class SolverTest {

    /**
     * A single test sample.
     */
    @SuppressWarnings({"PMD.CommentRequired", "PMD.ArrayIsStoredDirectly"})
    private static class Sample {
        final String[] input;
        final HitOrMiss[] output;
        
        public Sample(String[] input, HitOrMiss[] output) {
            this.input = input;
            this.output = output;
        }

        @Override
        public String toString() {
            return String.join(",", Arrays.asList(input));
        }
    }
    
    private static Sample[] testSamples() {
        return new Sample[] {
            new Sample(
                new String[] {
                    "3x3",
                    "A B C",
                    "D E F",
                    "G H I",
                    "ABC",
                    "BEH",
                    "AEI",
                    "ABCD",
                    "Z",

                },
                new HitOrMiss[] {
                    new HitOrMiss("ABC", 0, 0, 0, 2),
                    new HitOrMiss("BEH", 0, 1, 2, 1),
                    new HitOrMiss("AEI", 0, 0, 2, 2),
                }),

            new Sample(
                new String[] {"0x0",},
                new HitOrMiss[] {}),

            new Sample(
                new String[] {
                    "1x1",
                    "A",
                    "A",
                    "B",
                    "",

                },
                new HitOrMiss[] {
                    new HitOrMiss("A", 0, 0, 0, 0),
                }),

            new Sample(
                new String[] {
                    "1x10",
                    "A B C D E F G H I J",
                    "a",
                    "ij",
                    "j",
                    "gfe",
                },
                new HitOrMiss[] {
                    new HitOrMiss("a", 0, 0, 0, 0),
                    new HitOrMiss("ij", 0, 8, 0, 9),
                    new HitOrMiss("j", 0, 9, 0, 9),
                    new HitOrMiss("gfe", 0, 6, 0, 4),
                }),

            new Sample(
                new String[] {
                    "10x1",
                    "a",
                    "b",
                    "c",
                    "d",
                    "e",
                    "f",
                    "g",
                    "h",
                    "i",
                    "j",
                    "A",
                    "J",
                    "Cd E",
                    "e Dc",
                    "hg  fe dc b",
                },
                new HitOrMiss[] {
                    new HitOrMiss("A", 0, 0, 0, 0),
                    new HitOrMiss("J", 9, 0, 9, 0),
                    new HitOrMiss("Cd E", 2, 0, 4, 0),
                    new HitOrMiss("e Dc", 4, 0, 2, 0),
                    new HitOrMiss("hg  fe dc b", 7, 0, 1, 0),
                }),

            new Sample(
                new String[] {
                    "5x5",
                    "H A S D F",
                    "G E Y B H",
                    "J K L Z X",
                    "C V B L N",
                    "G O O D O",
                    "l l o",
                    "yz n",
                    "j vo",
                    "Fblvg",
                    "blvg",
                    "lbf",
                    "bly",
                },
                new HitOrMiss[] {
                    new HitOrMiss("l l o", 2, 2, 4, 4),
                    new HitOrMiss("yz n", 1, 2, 3, 4),
                    new HitOrMiss("j vo", 2, 0, 4, 2),
                    new HitOrMiss("Fblvg", 0, 4, 4, 0),
                    new HitOrMiss("blvg", 1, 3, 4, 0),
                    new HitOrMiss("lbf", 2, 2, 0, 4),
                    new HitOrMiss("bly", 3, 2, 1, 2),
                }),

        };
    }

    @ParameterizedTest
    @MethodSource("testSamples")
    void test(Sample sample) {
        HitOrMiss[] actual = new Solver()
            .solve(Stream.of(sample.input))
            .toArray(HitOrMiss[]::new);

        assertArrayEquals(sample.output, actual);
    }

}
