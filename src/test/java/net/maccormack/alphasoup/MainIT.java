package net.maccormack.alphasoup;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.regex.Pattern;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

/**
 * Integration test for Main.
 * @author dmaccormack
 */
class MainIT {

    /**
     * Input filename extension.
     */
    private static final String TEST_INPUT_EXT = ".input.txt";
    
    /**
     * Output filename extension.
     */
    private static final String TEST_OUTPUT_EXT = ".output.txt";

    /**
     * Regex that captures space(s) to normalize for test assertion. 
     */
    private static final Pattern NORMALIZE_PATTERN = Pattern.compile(
        "[ \t\r\n]+");

    /**
     * Path to test resource directory.
     */
    private static final Path RESOURCE_PATH = Paths.get(
        "src", "test", "resources");

    /**
     * Normalize string space(s).
     */
    private static String normalize(String text) {
        return NORMALIZE_PATTERN.matcher(text).replaceAll(" ");
    }
    
    @ParameterizedTest
    @ValueSource(strings = {
        "test1",
        "test2",
    })
    @SuppressWarnings("java:S2699")
    void testMain(String name) throws IOException {
        System.setIn(Files.newInputStream(
            RESOURCE_PATH.resolve(name + TEST_INPUT_EXT)));

        ByteArrayOutputStream bytes = new ByteArrayOutputStream(1 << 12);
        System.setOut(new PrintStream(bytes, false, Main.FILE_CHARSET_NAME));
        
        Main.main(new String[] {});

        String actual = normalize(bytes.toString(Main.FILE_CHARSET_NAME));

        String expected = normalize(new String(Files.readAllBytes(
            RESOURCE_PATH.resolve(name + TEST_OUTPUT_EXT)), Main.FILE_CHARSET));
        
        assertEquals(expected, actual);
    }

}
